import Vue from 'vue';
import Vuex from 'vuex';
import products from './products'
import common from './common'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    products, common
  },
  strict: process.env.NODE_ENV !== 'production'
})
