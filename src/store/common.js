export default {
  namespaced: true,
  state:{
    loadingState: false,
    loading: false,
    message: {
      success: '',
      error: ''
    }
  },
  mutations: {
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
    SET_LOADING_STATE(state, payload) {
      state.loadingState = payload;
    },
    SET_MESSAGE_SUCCESS(state, message) {
      state.message.success = message;
    },
    SET_MESSAGE_ERROR(state, message) {
      state.message.error = message;
    },
    CLEAR_MESSAGE(state) {
      state.message.success = '';
      state.message.error = '';
    }
  },
  actions: {
    SET_LOADING({commit}, payload) {
      commit('SET_LOADING', payload);
    },
    SET_LOADING_STATE({commit}, payload) {
      commit('SET_LOADING_STATE', payload);
    },
    SET_MESSAGE_SUCCESS({commit}, message) {
      commit('SET_MESSAGE_SUCCESS', message);
    },
    SET_MESSAGE_ERROR({commit}, message) {
      commit('SET_MESSAGE_ERROR', message);
    },
    CLEAR_MESSAGE({commit}) {
      commit('CLEAR_MESSAGE');
    }
  },
  getters: {
    LOADING (state) {
      return state.loading;
    },
    LOADING_STATE (state) {
      return state.loadingState;
    },
    MESSAGE_SUCCESS(state) {
      return state.message.success;
    },
    MESSAGE_ERROR(state) {
      return state.message.error;
    }
  }
}