import * as API from "@/request";

export default {
  namespaced: true,
  state: {
    products: [],
    selected_products: []
  },
  mutations: {
    SET_PRODUCTS(state, products) {
      state.products = products;
    },
    SORT_PRODUCTS(state, {col, dir}) {
      if(typeof state.products[0][col] === 'string') {
        state.products.sort((a, b) => dir * a[col].localeCompare(b[col]));
      } else {
        state.products.sort((a, b) => dir * (a[col] - b[col]));
      }
    },
    SELECT_PRODUCT(state, id) {
      let index = state.selected_products.indexOf(id);
      if(index === -1) state.selected_products.push(id);
    },
    UNSELECT_PRODUCT(state, id) {
      let index = state.selected_products.indexOf(id);
      if(index !== -1) state.selected_products.splice(index, 1);
    },
    DELETE_PRODUCTS(state, id) {
      let arr = id ? [id] : state.selected_products;
      arr.forEach(item => {
        let index = state.products.findIndex(product => product.id === item);
        state.products.splice(index, 1);
      });
      state.selected_products = [];

    }
  },
  actions: {
    GET_PRODUCTS({commit}) {
      commit('common/SET_LOADING_STATE', true, {root: true});
      API.getProducts().then(responce => {
        commit('SET_PRODUCTS', responce);
        commit('common/SET_LOADING_STATE', false, {root: true});
      }, error => {
        commit('common/SET_LOADING_STATE', false, {root: true});
        commit('common/SET_MESSAGE_ERROR', error.error, {root: true});
      })
    },
    DELETE_PRODUCTS({commit}, id) {
      return new Promise((resolve, reject) => {
        commit('common/CLEAR_MESSAGE', '', {root: true});
        commit('common/SET_LOADING', true, {root: true});
        API.deleteProducts().then(responce => {
          commit('DELETE_PRODUCTS', id);
          commit('common/SET_LOADING', false, {root: true});
          commit('common/SET_MESSAGE_SUCCESS', responce.message, {root: true});
          resolve();
        }, error => {
          commit('common/SET_LOADING', false, {root: true});
          commit('common/SET_MESSAGE_ERROR', error.error, {root: true});
          resolve();
        });
      });
    },
    SORT_PRODUCTS({commit}, col, dir) {
      commit('SORT_PRODUCTS', col, dir);
    },
    SELECT_PRODUCT({commit}, id) {
      commit('SELECT_PRODUCT', id);
    },
    UNSELECT_PRODUCT({commit}, id) {
      commit('UNSELECT_PRODUCT', id);
    }
  },
  getters: {
    PRODUCTS(state) {
      return state.products;
    },
    SELECTED_PRODUCTS(state) {
      return state.selected_products;
    }
  }
}